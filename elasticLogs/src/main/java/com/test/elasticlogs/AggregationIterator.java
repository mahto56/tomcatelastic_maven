/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregation;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
import org.elasticsearch.search.builder.SearchSourceBuilder;

/**
 *
 * @author arvind-pt3044
 */
    public class AggregationIterator implements Iterator<List<ParsedComposite.ParsedBucket>> {
        
        SearchRequest searchRequest;
        RestHighLevelClient client;
        SearchSourceBuilder searchSource;
        Map<String, Object> afterKey;
        List<ParsedComposite.ParsedBucket> buckets;
        CompositeAggregationBuilder compAggr;
        SearchResponse searchResponse;
        private CompositeAggregation resultAggr;
        
        public AggregationIterator(String indexName, SearchSourceBuilder searchSource, CompositeAggregationBuilder compAggr, RestHighLevelClient client) throws IOException {
            this.searchRequest = new SearchRequest(indexName)
                    .source(searchSource
                            .size(0)
                            .trackTotalHits(false)
                    );
            
            this.searchSource = searchSource;
            this.compAggr = compAggr;
            this.searchSource.aggregation(compAggr);
            this.client = client;
            searchResponse = this.client.search(searchRequest, RequestOptions.DEFAULT);
            resultAggr = (CompositeAggregation) searchResponse.getAggregations().asList().get(0);
            buckets = ((ParsedComposite) resultAggr).getBuckets();
            afterKey = resultAggr.afterKey();
            compAggr.size(1000); //size to retrieve at a time
        }
        
        @Override
        public boolean hasNext() {
            return buckets != null && buckets.size() > 0;
        }
        
        @Override
        public List<ParsedComposite.ParsedBucket> next() {
            
            List<ParsedComposite.ParsedBucket> buckets_copy = buckets.stream().collect(Collectors.toList());
            compAggr.aggregateAfter(afterKey);
            
            //searchSource.aggregation(compAggr);
            searchRequest.source(searchSource);
            try {
                searchResponse = this.client.search(searchRequest, RequestOptions.DEFAULT);
            } catch (IOException ex) {
                Logger.getLogger(LogsUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
            resultAggr = (CompositeAggregation) searchResponse.getAggregations().asList().get(0);
            buckets = ((ParsedComposite) resultAggr).getBuckets();
            afterKey = resultAggr.afterKey();
            
            return buckets_copy;
            
        }
        
    }