/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author arvind-pt3044
 */
public class AltLogFileReader  {

    private final List<LogBean> lf_list;
    private final Pattern p;
    private final File f;
    private final BufferedReader bf;
    private String nextLine;
    private BigInteger lineNo;
    private final int readSize;

    AltLogFileReader(String filename, int readSize) throws FileNotFoundException, IOException {
        f = new File(filename);
        bf = new BufferedReader(new FileReader(f));
        lf_list = new ArrayList();
        p = Pattern.compile("^([\\S.:]+) (\\S+) (\\S+) \\[(\\d+\\/\\S+\\/\\d{4}:\\d{2}:\\d{2}:\\d{2} [-+]\\d{4})\\] \\\"([\\S ]+)\\\" (\\d{3}) (-*\\d*)"); //regex pattern for log file
        nextLine = bf.readLine();
        lineNo = BigInteger.ZERO;
        this.readSize = readSize;
    }

    public boolean hasNext() {
        return this.nextLine != null;
    }

    public List<LogBean> getNext() throws Exception {
        this.lf_list.clear();
        int lineCount = 0;
        while (lineCount < this.readSize && this.nextLine != null) {
            Matcher m = p.matcher(this.nextLine);
            ++lineCount;
            this.lineNo = this.lineNo.add(BigInteger.ONE);
            if (m.find()) {
                String logid = this.f.getName() + "__" + lineNo;
                LogBean item = new LogBean(logid, m.group(1), m.group(2), m.group(3), m.group(4), m.group(5), m.group(6), m.group(7));
                this.lf_list.add(item);
            } else {
                throw new Exception("Log patterns doesn't matches at line no: " + lineNo);
            }
            this.nextLine = this.bf.readLine();
        }
        return this.lf_list;
    }

    
//test code    
//    public static void main(String args[]) throws IOException, Exception {
//        String filename = "E:\\LargeFileTest\\l.txt";
//        AltLogFileReader altReader = new AltLogFileReader(filename, 1000);
//        while (altReader.hasNext()) {
//            for (LogBean lb : altReader.getNext()) {
////                System.out.println(lb);
//            }
//
//        }
//        System.out.println("done");
//    }

}
