/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import java.util.Random;

/**
 *
 * @author arvind-pt3044
 */
class Generator {
    Random r;
    String arr;

    public Generator() {
        this.r = new Random();
        this.arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_"; 
    }
    
    
    String getRandomHostName() {
        int size = 7;
        StringBuilder randString = new StringBuilder(size);
        for(int i=0;i<size;i++){
            randString.append(arr.charAt(r.nextInt(arr.length())));
        }
        return "www."+randString.toString()+".com";
    }
    
}
