/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeValuesSourceBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
import org.elasticsearch.search.aggregations.metrics.ValueCount;

import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.sort.SortOrder;

/**
 *
 * @author arvind-pt3044
 */
public class LogsUtil {
    
    private final Gson gson;
    private final RestHighLevelClient client;
    private final Generator hostNameGenerator;
    HashSet<String> currIPSet;
    
    public LogsUtil(RestHighLevelClient client) {
        this.client = client;
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        hostNameGenerator = new Generator();
        currIPSet = new HashSet<>(); //to not query the server for duplicate IP
    }

//    adds all logs from file: filename to index: indexName 
//    and if new IP found adds it to index: hostIndex in bulk of size: readSize at a time.
    void addLogsToElastic(String filename, String indexName, String hostIndex, int readSize) throws Exception {
        AltLogFileReader altReader = new AltLogFileReader(filename, readSize);
        System.out.println("Adding logs from file: " + filename + " to index: " + indexName);
        System.out.println("Chunk size: " + readSize + "\n\n");
        
        BigInteger total = BigInteger.ZERO;
        while (altReader.hasNext()) {
            BulkRequest bulkRequest = new BulkRequest(indexName);
            int newIPsFound = 0;
            
            if (currIPSet.size() > readSize * 10) { // if currIP gets too large rebuild it
                currIPSet.clear();
            }
            
            for (LogBean lb : altReader.getNext()) {
                String jsonData = gson.toJson(lb);
                IndexRequest indexRequest = new IndexRequest(indexName).id(lb.getID()).source(jsonData, XContentType.JSON);
                bulkRequest.add(indexRequest);

                //check if IP in present in current set of already inserted IPs
                //and check if IP doesnt exist on index,
                //if both false, then add to current Set, and add to index
                if (!currIPSet.contains(lb.getClientIP())) {
                    if (!ipExistsInIndex(lb.getClientIP(), hostIndex)) {
                        System.out.println("new IP found: " + lb.getClientIP());
                        String hostName = hostNameGenerator.getRandomHostName();
                        IpHostBean hb = new IpHostBean(lb.getClientIP(), hostName);
                        jsonData = gson.toJson(hb);
                        IndexRequest hostIndexRequest = new IndexRequest(hostIndex)
                                .id(hb.getClientIP())
                                .source(jsonData, XContentType.JSON);
                        bulkRequest.add(hostIndexRequest); //add hostIndexRequest to bulkRequest
                        ++newIPsFound;
                    }
                    currIPSet.add(lb.getClientIP()); //seen

                }
                
            }
            
            int curSize = readSize;
            BulkResponse resp = this.client.bulk(bulkRequest, RequestOptions.DEFAULT);
            System.out.println("Status Code: " + resp.status().toString());
            if (RestStatus.OK == resp.status()) {
                total = total.add(BigInteger.valueOf(curSize));
                System.out.println(total + " logs added till now...");
                if (newIPsFound != 0) {
                    System.out.println(newIPsFound + " new IPs added");
                }
            }
        }
        
    }

    //get total log count in index
    long getLogCount(String index) throws IOException {
        CountRequest countRequest = new CountRequest().
                indices(index).
                source(new SearchSourceBuilder().
                        query(QueryBuilders.matchAllQuery()
                        )
                );
        CountResponse countResponse = this.client.count(countRequest, RequestOptions.DEFAULT);
        return countResponse.getCount();
        
    }



    //search by hostname
    SearchIterator searchByHostName(String hostname, String indexName) throws IOException {
        SearchSourceBuilder searchSource = new SearchSourceBuilder()
                .query(
                        QueryBuilders.matchQuery("clientIP", hostname)
                );
        return new SearchIterator(indexName, searchSource, client, 1000,this.gson);
    }

    //search a value in all fields of the index using multi_match query
    SearchIterator searchByValAllFields(String valToSearch, String indexName) throws IOException {
        SearchSourceBuilder searchSource = new SearchSourceBuilder()
                .query(
                        QueryBuilders.multiMatchQuery(valToSearch, "logID", "clientAPI", "clientIdentity", "clientUserID", "requestLine", "statusCode", "recievedBytesSize")
                                .lenient(true)
                );
        return new SearchIterator(indexName, searchSource, client, 1000,this.gson);
    }

    //all logs in index with date in range of fromDate to toDate
    SearchIterator searchByDate(String fromDate, String toDate, String format, String indexName) throws IOException {
        SearchSourceBuilder searchSource = new SearchSourceBuilder()
                .query(
                        QueryBuilders.rangeQuery("timeStamp")
                                .gte(fromDate)
                                .lte(toDate)
                                .format(format)
                );
        return new SearchIterator(indexName, searchSource, client, 1000,this.gson);
    }

    //all logs
    SearchIterator getAllLogs(String indexName, boolean sort) throws IOException {
        //use the search method , "query":{"match_all":{}}
        SearchSourceBuilder searchSource;
        if (sort) {
            searchSource = new SearchSourceBuilder().
                    query(
                            QueryBuilders.matchAllQuery()
                    ).sort("timeStamp", SortOrder.ASC);
        } else {
            searchSource = new SearchSourceBuilder().
                    query(
                            QueryBuilders.matchAllQuery()
                    );
        }
        return new SearchIterator(indexName, searchSource, client, 1000,this.gson);
    }
    
    
    SearchIterator searchByField(String searchTerm, String indexName, String field) throws IOException {
        SearchSourceBuilder searchSource;
        searchSource = new SearchSourceBuilder()
                .query(QueryBuilders.termQuery(field, searchTerm));
        return new SearchIterator(indexName, searchSource, client, 1000,this.gson);
    }
    
    long TermFrequencyAggregation(String searchTerm, String indexName,String field) throws IOException {
        String aggfield=field;
        SearchSourceBuilder searchSource;
        
        if(field.equals("requestLine"))
        {   
            aggfield = "requestLine.raw";
        }
        
        searchSource = new SearchSourceBuilder()
                .query(QueryBuilders.termQuery(field, searchTerm)).size(0);
        searchSource.aggregation(AggregationBuilders.count("myagg").field(aggfield));
        SearchRequest searchRequest = new SearchRequest(indexName).source(searchSource);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        ValueCount vc = searchResponse.getAggregations().get("myagg");
        return vc.getValue();
        
    }
    

    //delete log with id
    void deleteByID(String id, String indexName) throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest(indexName, id);
        DeleteResponse deleteResponse = this.client.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println("Status Code: " + deleteResponse.status().toString());
    }

    //returns true if ip exists in the index , else false
    boolean ipExistsInIndex(String ip, String indexName) throws IOException {
        String id = ip;
        GetRequest getRequest = new GetRequest(indexName, id)
                .fetchSourceContext(FetchSourceContext.DO_NOT_FETCH_SOURCE);
        GetResponse getResponse = this.client.get(getRequest, RequestOptions.DEFAULT);
        return getResponse.isExists();
    }
    
    AggregationIterator getTotalBytesPerIPAggregationIterator(String indexName) throws IOException {
        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
        TermsValuesSourceBuilder clientIP = new TermsValuesSourceBuilder("clientIP")
                .field("clientIP");
        sources.add(clientIP);
        CompositeAggregationBuilder cagg = new CompositeAggregationBuilder("results", sources);
        cagg.subAggregation(AggregationBuilders.sum("totalBytes").field("recievedBytesSize"));
        SearchSourceBuilder searchSource = new SearchSourceBuilder();
        return new AggregationIterator(indexName, searchSource, cagg, client);
        
    }
    
    AggregationIterator getStatusCodeAggregationIterator(String indexName, List<Integer> statusCodeList) throws IOException {
        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
        TermsValuesSourceBuilder clientIP = new TermsValuesSourceBuilder("statusCode")
                .field("statusCode");
        sources.add(clientIP);
        CompositeAggregationBuilder cagg = new CompositeAggregationBuilder("results", sources);
        SearchSourceBuilder searchSource = new SearchSourceBuilder()
                .query(
                //        QueryBuilders.termsQuery("statusCode", statusCodeList)
                        QueryBuilders.matchAllQuery()
                );
        return new AggregationIterator(indexName, searchSource, cagg, client);
        
    }

    


    
}
