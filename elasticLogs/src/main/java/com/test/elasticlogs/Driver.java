/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
import org.elasticsearch.search.aggregations.metrics.Sum;

/**
 *
 * @author arvind-pt3044
 */
public class Driver {

    public static void main(String s[]) throws IOException, Exception {
        try (
                //initiate rest-client for elasticsearch
                RestHighLevelClient client = new RestHighLevelClient(
                        RestClient.builder(
                                new HttpHost("localhost", 9200, "http"))
                )) {
            String mainIndex = "casesensitive_index";
            String hostnameIndex = "hostname";
            LogsUtil utility = new LogsUtil(client);
            List<Integer> statusCodes = new ArrayList<>();
            SearchIterator logsSearchIterator;
            long count = utility.getLogCount(mainIndex);

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Total Documents in index: " + mainIndex + ": " + count);
            while (true) {
                System.out.println("\n----------------------------------");
                System.out.println("ElasticSearch Tomcat Logs Util");
                System.out.println("1. View data from elasticsearch");
                System.out.println("2. Add data from logfile");
                System.out.println("3. Delete data by ID from elasticsearch");
                System.out.println("4. Search by Hostname/IP");
                System.out.println("5. Search by Date");
                System.out.println("6. Search in all Fields");
                System.out.println("7. Status Code frequency");
                System.out.println("8. Total bytes received for each clientIP ");
                System.out.println("9. Search on any field [CASE SENSITIVE], with total count the search term appears");
                System.out.println("10. Quit");
                
                System.out.println("Enter your choice: ");
                int ch = Integer.parseInt(in.readLine());
                switch (ch) {
                    case 1:
                        logsSearchIterator = utility.getAllLogs(mainIndex, false);
                        long start = System.currentTimeMillis();
                        printSearchResult(logsSearchIterator);
                        long end = System.currentTimeMillis();
//                        System.out.println("Total time taken: " + ((end - start) / 1000.0) + " s");
                        break;
                    case 2:
                        System.out.println("Enter file path: ");
                        String path = in.readLine();
                        int readSize = 1000;
                        utility.addLogsToElastic(path, mainIndex, hostnameIndex, readSize);
                        break;
                    case 3:
                        System.out.println("Enter logID: ");
                        String logID = in.readLine();
                        utility.deleteByID(logID, mainIndex);
                        break;
                    case 4:
                        System.out.println("Enter Host/IP");
                        String hostname = in.readLine();
                        logsSearchIterator = utility.searchByHostName(hostname, mainIndex);
                        printSearchResult(logsSearchIterator);
                        break;
                    case 5:
                        String format = "dd/MM/yyyy";
                        System.out.println("Enter date in format: " + format);
                        System.out.println("Enter Start date: ");
                        String from = in.readLine();
                        System.out.println("Enter End date: ");
                        String to = in.readLine();
                        logsSearchIterator = utility.searchByDate(from, to, format, mainIndex);
                        printSearchResult(logsSearchIterator);
                        break;
                    case 6:
                        System.out.println("Enter value to search");
                        String val = in.readLine();
                        logsSearchIterator = utility.searchByValAllFields(val, mainIndex);
                        printSearchResult(logsSearchIterator);
                        break;

                    case 7:
                        AggregationIterator it = utility.getStatusCodeAggregationIterator(mainIndex, statusCodes);
                        System.out.println(String.format("%-10s : %-10s", "StatusCode", "Count"));
                        while (it.hasNext()) {
                            List<ParsedComposite.ParsedBucket> l = it.next();
                            l.forEach((ip) -> {
                                System.out.println(String.format("%-10s : %-10s",ip.getKey().get("statusCode"),Double.valueOf(ip.getDocCount()).longValue()));
                            });
                        }
                        break;

                    case 8:
                        System.out.println(String.format("%-25s : %-20s", "IP Address", "Total Bytes"));
                        //agregation testing block
                        it = utility.getTotalBytesPerIPAggregationIterator(mainIndex);
                        count = 0;
                        while (it.hasNext()) {
                            List<ParsedComposite.ParsedBucket> l = it.next();
                            for (ParsedComposite.ParsedBucket ip : l) {
                                Sum totalBytes = ip.getAggregations().get("totalBytes");
                                System.out.println(String.format("%-25s : %-20d", ip.getKey().get("clientIP"), Double.valueOf(totalBytes.getValue()).longValue()));
                                count++;
                            };

                        }
                        break;
                    case 9:
                        System.out.println("Choose field:");
                        String[] list = {"logID","clientIP","clientIdentity","clientUserID","timeStamp","requestLine","statusCode","recievedBytesSize"};
                        for(int i=0;i<list.length;i++){
                            System.out.println(String.format("%d) %-20s",i,list[i]));
                        }
                        System.out.println("Enter choice no: ");
                        int i = Integer.parseInt(in.readLine());
                        if(i>0 && i<list.length){
                            
                            System.out.println("Enter Search Term");
                            String searchTerm = in.readLine();
                            long valueCount = utility.TermFrequencyAggregation(searchTerm, mainIndex, list[i]);
                            logsSearchIterator =  utility.searchByField(searchTerm, mainIndex,list[i]);
                            printSearchResult(logsSearchIterator);
                            System.out.println("Aggreggation Resut: "+valueCount);
                            
                        }else{
                            System.out.println("Invalid choice!");
                            
                        }
                        break;
                        
                    case 10:
                        return;
                    default:
                        System.out.println("invalid choice. choices are: 1,2,3,4,5,6");
                }

            }
        }

    }

    private static void printSearchResult(SearchIterator logsSearchIterator) throws IOException {
        long count = 0;
        while (logsSearchIterator.hasNext()) {
            List<LogBean> loglist = logsSearchIterator.next();
            if (loglist.isEmpty()) {
                System.out.println("No results found! ");
            } else {
                for (int i = 0; i < loglist.size(); i++) {
                    System.out.println(loglist.get(i));
                    count++;
                }

            }
        }
//        System.out.println(count + " results found!");
    }

}
