/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;


/**
 *
 * @author arvind-pt3044
 */
public class LogBean {
    private String  logID;
    private String clientIP;
    private String clientIdentity;
    private String clientUserID;
    private String timeStamp;
    private String requestLine;
    private int statusCode;
    private int recievedBytesSize;
    
    public LogBean(String logID,String clientIP,String clientIdentity,String clientUserID,String timeStamp,String requestLine,String statusCode,String recievedBytesSize){
        this.logID = logID;
        this.clientIP =  clientIP;
        this.clientIdentity = clientIdentity;
        this.clientUserID =  clientUserID;
        this.timeStamp = timeStamp;
        this.requestLine = requestLine;
        this.statusCode = Integer.parseInt(statusCode);
        this.recievedBytesSize = recievedBytesSize.equals("-")?0:Integer.parseInt(recievedBytesSize);
    }

    public void setLogID(String logID) {
        this.logID = logID;
    }

    public String getID() {
        return logID;
    }
    
    public String getClientIP() {
        return clientIP;
    }

    @Override
    public String toString() {
        return String.format("%-40s:   %-25s %-3s %-8s %-30s %-72s %-5s %-5s",this.logID,this.clientIP,this.clientIdentity,this.clientUserID,this.timeStamp,this.requestLine,this.statusCode,this.recievedBytesSize);
    }
}
