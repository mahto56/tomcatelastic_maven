/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author arvind-pt3044
 */
public class LogFileReader {
    List<LogBean> lf_list;
    private final Pattern p;
    LogFileReader(){
        lf_list = new ArrayList();
        p = Pattern.compile("^([\\S.:]+) (\\S+) (\\S+) \\[(\\d+\\/\\S+\\/\\d{4}:\\d{2}:\\d{2}:\\d{2} [-+]\\d{4})\\] \\\"([\\S ]+)\\\" (\\d{3}) (-*\\d*)"); //regex pattern
        
    }
    public List<LogBean> getLogList(String filename) throws IOException, Exception{
        lf_list.clear();
        File f = new File(filename);
        BufferedReader bf = new BufferedReader(new FileReader(f));
        String s;
       
        
        int lineno = 0;
        while((s = bf.readLine())!=null){
            Matcher m = p.matcher(s);
            lineno++;
            if(m.find()){
                String logid =  f.getName()+"__"+lineno;
                LogBean item = new LogBean(logid, m.group(1),m.group(2),m.group(3),m.group(4),m.group(5),m.group(6),m.group(7));
                lf_list.add(item);
                System.out.println(item);
            }else{
                throw new Exception("Log patterns doesn't matches at line no: "+lineno);
            }
        }
        
        return lf_list;
    }

//test code    
//    public static void main(String args[]) throws Exception{
//        LogFileReader lr = new LogFileReader();
//        lr.getLogList("E:\\LargeFileTest\\dummy2.txt");
//    
//    }
    

    
}
