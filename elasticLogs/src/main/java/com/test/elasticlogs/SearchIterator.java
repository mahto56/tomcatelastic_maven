/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

/**
 *
 * @author arvind-pt3044
 */
//class to handle scrolling  results
    public class SearchIterator implements Iterator<List<LogBean>> {
        
        Scroll scroll;
        SearchRequest searchRequest;
        RestHighLevelClient client;
        SearchResponse searchResponse;
        String scrollID;
        SearchHit[] hits;
        ArrayList<LogBean> lf;
        int readSize;
    private final Gson gson;
        
        public SearchIterator(String indexName, SearchSourceBuilder searchSource, RestHighLevelClient client, int readSize,Gson gson) throws IOException {
            this.scroll = new Scroll(TimeValue.timeValueMinutes(1L));
            this.searchRequest = new SearchRequest(indexName)
                    .source(searchSource
                            .size(readSize)
                    )
                    .scroll(scroll);
            this.client = client;
            searchResponse = this.client.search(searchRequest, RequestOptions.DEFAULT);
            scrollID = searchResponse.getScrollId();
            hits = searchResponse.getHits().getHits();
            lf = new ArrayList<>();
            this.readSize = readSize;
            this.gson = gson;
        }
        
        @Override
        public boolean hasNext() {
            return hits != null && hits.length > 0;
        }
        
        @Override
        public List<LogBean> next() {
            lf.clear();
            int itemCount = 0;
            while (itemCount < this.readSize && hasNext()) {
                for (SearchHit hit : hits) {
                    String json = hit.getSourceAsString();
                    LogBean obj = gson.fromJson(json, LogBean.class);
                    lf.add(obj);
                    itemCount++;
                }
                
                SearchScrollRequest ssr = new SearchScrollRequest(scrollID);
                ssr.scroll(scroll);
                try {
                    this.searchResponse = this.client.scroll(ssr, RequestOptions.DEFAULT);
                } catch (IOException ex) {
                    Logger.getLogger(LogsUtil.class.getName()).log(Level.SEVERE, null, ex);
                    throw new RuntimeException(ex);
                }
                this.scrollID = searchResponse.getScrollId();
                hits = searchResponse.getHits().getHits();
            }
            
            if (!hasNext()) {
                try {
                    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
                    clearScrollRequest.addScrollId(scrollID);
                    ClearScrollResponse clearScrollResponse = this.client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
                    boolean succeeded = clearScrollResponse.isSucceeded();
                    System.out.println("Succeeded: " + succeeded + ", Status code: " + searchResponse.status());
                } catch (IOException ex) {
                    Logger.getLogger(LogsUtil.class.getName()).log(Level.SEVERE, null, ex);
                    throw new RuntimeException(ex);
                }
            }
            
            return this.lf;
            
        }
        
    }
    