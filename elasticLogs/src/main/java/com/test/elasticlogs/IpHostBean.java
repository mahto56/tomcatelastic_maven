/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.elasticlogs;

/**
 *
 * @author arvind-pt3044
 */
class IpHostBean {
    String clientIP;
    String hostName;

    public IpHostBean(String clientIP, String hostName) {
        this.clientIP = clientIP;
        this.hostName = hostName;
    }

    public String getClientIP() {
        return clientIP;
    }
    
    
}
